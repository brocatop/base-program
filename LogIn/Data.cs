﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogIn
{
    public class Data
    {
        public string pos { get; set; }
        public float Tm1 { get; set; }
        public float Area1 { get; set; }
        public float Tm2 { get; set; }
        public float Area2 { get; set; }
        public Data(string Pos, float tm1, float area1, float tm2, float area2)
        {
            pos = Pos;
            Tm1 = tm1;
            Area1 = area1;
            Tm2 = tm2;
            Area2 = area2;
        }
    }
}
