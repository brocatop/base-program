﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using MRG.Properties;
using Excel = Microsoft.Office.Interop.Excel;

namespace LogIn
{
    public partial class PreviewReportcs : Form
    {
        //Interface formatting
        public PreviewReportcs()
        {
            InitializeComponent();
            ExportReportButton.Focus();
            dataGridView1.Anchor = AnchorStyles.Bottom | AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Left;
            ExportReportButton.Anchor = AnchorStyles.Right;
            SaveReportButton.Anchor = AnchorStyles.Right;
            CancelButton.Anchor = AnchorStyles.Right;
            MinimizeBox = false;
        }
        private void CopyDataTable()
        {
            //Copies the contents of the dataGridView for export
            dataGridView1.SelectAll();
            DataObject dataObj = dataGridView1.GetClipboardContent();
            if (dataObj != null)
            {
                Clipboard.SetDataObject(dataObj);
            }
            else
            {
                MessageBox.Show("The current report is empty. Cannot Export the report. Please make sure the report is valid.");
            }
        }

        //Closes the preview report page
        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Updates the changes made.
        private void SaveReportButton_Click(object sender, EventArgs e)
        {
            //This will be an SQL query that copies the dtatatable, including presumed changes, and updates the db
            DataTable dt = (DataTable)(dataGridView1.DataSource);
        }

        //Exports the report
        private void ExportReportButton_Click(object sender, EventArgs e)
        {
            ExportToExcel();            
        }

        //Copies the data of the dataGridView and pastes it into a new Excel File.
        private void ExportToExcel()
        {
            SaveFileDialog sf = new SaveFileDialog();
            sf.Filter = "Excel (*.xls)|*.xls";
            if (sf.ShowDialog() == DialogResult.OK)
            {
                if (!sf.FileName.Equals(String.Empty))
                {
                    FileInfo f = new FileInfo(sf.FileName);
                    if (f.Extension.Equals(".xls"))
                    {
                        Excel.Application xlApp;
                        Excel.Workbook xlWorkBook;
                        Excel.Worksheet xlWorkSheet;
                        object misValue = System.Reflection.Missing.Value;
                        xlApp = new Excel.Application();
                        xlWorkBook = xlApp.Workbooks.Add(misValue);
                        xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                        int i = 0;
                        int j = 0;
                    
                        for (i = 0; i <= dataGridView1.RowCount - 1; i++)
                        {
                            for (j = 0; j <= dataGridView1.ColumnCount - 1; j++)
                            {

                                DataGridViewCell cell = dataGridView1[j, i];
                                xlWorkSheet.Cells.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                                xlWorkSheet.Columns.AutoFit();
                                if (cell.Value.GetType() == typeof(Bitmap))
                                {
                                    // You have to get original bitmap path here
                                    string path = Path.GetTempPath();
                                    MRG.Properties.Resources._957_a.Save(path);
    //                                string imagString = "bitmap1.bmp";
                                    Excel.Range oRange = (Excel.Range)xlWorkSheet.Cells[i + 1, j + 1];
                                    float Left = (float)((double)oRange.Left);
                                    float Top = (float)((double)oRange.Top);
                                    const float ImageSize = 32;
                                    xlWorkSheet.Shapes.AddPicture(path, Microsoft.Office.Core.MsoTriState.msoFalse, Microsoft.Office.Core.MsoTriState.msoCTrue, Left, Top, ImageSize, ImageSize);
                                    oRange.RowHeight = ImageSize + 2;
                                }
                                else
                                {
                                    xlWorkSheet.Cells[i + 1, j + 1] = cell.Value;
                                }

                            }
                        }

                        xlWorkBook.SaveAs(sf.FileName, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                        xlWorkBook.Close(true, misValue, misValue);
                        xlApp.Quit();

                        releaseObject(xlWorkSheet);
                        releaseObject(xlWorkBook);
                        releaseObject(xlApp);

                        MessageBox.Show("Excel file created , you can find the file " + sf.FileName);
                    }
                    else
                    {
                        MessageBox.Show("Invalid file type");
                    }
                }
                else
                {
                    MessageBox.Show("You did pick a location " +
                                    "to save file to");
                }
            }

            void releaseObject(object obj)
            {
                try
                {
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                    obj = null;
                }
                catch (Exception ex)
                {
                    obj = null;
                    MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
                }
                finally
                {
                    GC.Collect();
                }
            }

            /* CopyDataTable();
             string imagString = "bitmap1.bmp";
             Microsoft.Office.Interop.Excel.Application xlexcel;
             Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
             Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
             Microsoft.Office.Interop.Excel.Range oRange = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.Cells[i + 1, J + I];
             object misValue = System.Reflection.Missing.Value;
             xlexcel = new Microsoft.Office.Interop.Excel.Application();
             xlexcel.Visible = true;
             xlWorkBook = xlexcel.Workbooks.Add(misValue);
             xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
             Microsoft.Office.Interop.Excel.Range CR = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.Cells[1, 1];
             CR.Select();
             xlWorkSheet.PasteSpecial(CR, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true); */
        }

        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        //When the page loads, populates the dataGridView with the contents of a file
        private void PreviewReportcs_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            string f = @"C:\Users\patrick.brocato\Desktop\Projects\GTCA\4834 Bridges\4834 Bridges\Ppara\126 114.txt";
            string data = File.ReadAllText(f);
            //Tables for the Report
               dt.Columns.AddRange(new DataColumn[]
               {
                   new DataColumn("Include"),
                   new DataColumn("Color"),
                   new DataColumn("Pos"),
                   new DataColumn("Name"),
                   new DataColumn("Tm1"),
                   new DataColumn("Area1"),
                   new DataColumn("Tm2"),
                   new DataColumn("Area2"),
                   new DataColumn("Status"),
               });
            foreach (string row in data.Split('\n'))
            {
                if (!string.IsNullOrEmpty(row))
                {
                    dt.Rows.Add();
                    int i = 0;
                    foreach (string cell in row.Split('\t'))
                    {
                        dt.Rows[dt.Rows.Count - 1][i] = cell;
                        i++;
                    }
                }
            }
            

            dt.Columns.Add("Image", typeof(byte[]));
            Image image = Resources._957_a;
            DataRow dr = dt.NewRow();
            dr["Image"] = ImageToByteArray(image);
            dt.Rows.Add(dr);
            dataGridView1.DataSource = dt;
            ((DataGridViewImageColumn)this.dataGridView1.Columns["Image"]).DefaultCellStyle.NullValue = null;
            ((DataGridViewImageColumn)this.dataGridView1.Columns["Image"]).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            ((DataGridViewImageColumn)this.dataGridView1.Columns["Image"]).ImageLayout = DataGridViewImageCellLayout.Zoom;
            dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;

        }

        //Turns the uplaoded image into a byte array
        public byte[] ImageToByteArray(Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            return ms.ToArray();
        }
    }
}
