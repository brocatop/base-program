﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LogIn
{
    public partial class Profiles : Form
    {
        public Profiles()
        {
            InitializeComponent();
            MinimizeBox = false;
            MaximizeBox = false;
        }

        private void SaveProfileButton_Click(object sender, EventArgs e)
        {
            string mc = @"Server=(local)\SQLEXPRESS; Database=Geno; integrated security=SSPI;";
            using (SqlConnection sc = new SqlConnection(mc))
            {
                using (SqlCommand c = new SqlCommand("INSERT INTO Profiles(ProfileName, AssayType, MeltingTemp, Variance) VALUES(@pn, @at, @tt, @dt);", sc))
                {
                    sc.Open();
                    c.Parameters.AddWithValue("@pn", ProfileNameTextBox.Text);
                    c.Parameters.AddWithValue("@at", AssayTypeTextBox.Text);
                    c.Parameters.AddWithValue("@dt", DeviationTextBox.Text);
                    c.Parameters.AddWithValue("@tt", TemperatureTextBox.Text);
                    c.ExecuteNonQuery();
                    sc.Close();
                    MessageBox.Show("Profile has been successfully created!");
                }
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
