﻿namespace LogIn
{
    partial class Default_Paths
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UploadFilesPathLabel = new System.Windows.Forms.Label();
            this.UploadImagesPathLabel = new System.Windows.Forms.Label();
            this.ExportReportPathLabel = new System.Windows.Forms.Label();
            this.UploadImagesPathText = new System.Windows.Forms.TextBox();
            this.ExportReportPathText = new System.Windows.Forms.TextBox();
            this.UploadFilesPathText = new System.Windows.Forms.TextBox();
            this.BrowseButton2 = new System.Windows.Forms.Button();
            this.BrowseButton3 = new System.Windows.Forms.Button();
            this.SavePathsButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.BrowseButton1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // UploadFilesPathLabel
            // 
            this.UploadFilesPathLabel.AutoSize = true;
            this.UploadFilesPathLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.UploadFilesPathLabel.Location = new System.Drawing.Point(35, 81);
            this.UploadFilesPathLabel.Name = "UploadFilesPathLabel";
            this.UploadFilesPathLabel.Size = new System.Drawing.Size(162, 24);
            this.UploadFilesPathLabel.TabIndex = 0;
            this.UploadFilesPathLabel.Text = "Upload Files Path:";
            // 
            // UploadImagesPathLabel
            // 
            this.UploadImagesPathLabel.AutoSize = true;
            this.UploadImagesPathLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.UploadImagesPathLabel.Location = new System.Drawing.Point(35, 136);
            this.UploadImagesPathLabel.Name = "UploadImagesPathLabel";
            this.UploadImagesPathLabel.Size = new System.Drawing.Size(183, 24);
            this.UploadImagesPathLabel.TabIndex = 1;
            this.UploadImagesPathLabel.Text = "Upload Images Path:";
            // 
            // ExportReportPathLabel
            // 
            this.ExportReportPathLabel.AutoSize = true;
            this.ExportReportPathLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.ExportReportPathLabel.Location = new System.Drawing.Point(35, 189);
            this.ExportReportPathLabel.Name = "ExportReportPathLabel";
            this.ExportReportPathLabel.Size = new System.Drawing.Size(173, 24);
            this.ExportReportPathLabel.TabIndex = 2;
            this.ExportReportPathLabel.Text = "Export Report Path:";
            // 
            // UploadImagesPathText
            // 
            this.UploadImagesPathText.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.UploadImagesPathText.Location = new System.Drawing.Point(224, 138);
            this.UploadImagesPathText.Name = "UploadImagesPathText";
            this.UploadImagesPathText.Size = new System.Drawing.Size(203, 28);
            this.UploadImagesPathText.TabIndex = 3;
            // 
            // ExportReportPathText
            // 
            this.ExportReportPathText.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.ExportReportPathText.Location = new System.Drawing.Point(224, 189);
            this.ExportReportPathText.Name = "ExportReportPathText";
            this.ExportReportPathText.Size = new System.Drawing.Size(203, 28);
            this.ExportReportPathText.TabIndex = 4;
            // 
            // UploadFilesPathText
            // 
            this.UploadFilesPathText.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.UploadFilesPathText.Location = new System.Drawing.Point(224, 81);
            this.UploadFilesPathText.Name = "UploadFilesPathText";
            this.UploadFilesPathText.Size = new System.Drawing.Size(203, 28);
            this.UploadFilesPathText.TabIndex = 5;
            // 
            // BrowseButton2
            // 
            this.BrowseButton2.Location = new System.Drawing.Point(433, 139);
            this.BrowseButton2.Name = "BrowseButton2";
            this.BrowseButton2.Size = new System.Drawing.Size(75, 30);
            this.BrowseButton2.TabIndex = 7;
            this.BrowseButton2.Text = "Browse";
            this.BrowseButton2.UseVisualStyleBackColor = true;
            this.BrowseButton2.Click += new System.EventHandler(this.BrowseButton2_Click);
            // 
            // BrowseButton3
            // 
            this.BrowseButton3.Location = new System.Drawing.Point(433, 190);
            this.BrowseButton3.Name = "BrowseButton3";
            this.BrowseButton3.Size = new System.Drawing.Size(75, 30);
            this.BrowseButton3.TabIndex = 8;
            this.BrowseButton3.Text = "Browse";
            this.BrowseButton3.UseVisualStyleBackColor = true;
            this.BrowseButton3.Click += new System.EventHandler(this.BrowseButton3_Click);
            // 
            // SavePathsButton
            // 
            this.SavePathsButton.Location = new System.Drawing.Point(154, 277);
            this.SavePathsButton.Name = "SavePathsButton";
            this.SavePathsButton.Size = new System.Drawing.Size(114, 37);
            this.SavePathsButton.TabIndex = 9;
            this.SavePathsButton.Text = "Save Paths";
            this.SavePathsButton.UseVisualStyleBackColor = true;
            this.SavePathsButton.Click += new System.EventHandler(this.SavePathsButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Location = new System.Drawing.Point(333, 277);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(114, 37);
            this.CancelButton.TabIndex = 10;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // BrowseButton1
            // 
            this.BrowseButton1.Location = new System.Drawing.Point(433, 81);
            this.BrowseButton1.Name = "BrowseButton1";
            this.BrowseButton1.Size = new System.Drawing.Size(75, 30);
            this.BrowseButton1.TabIndex = 6;
            this.BrowseButton1.Text = "Browse";
            this.BrowseButton1.UseVisualStyleBackColor = true;
            this.BrowseButton1.Click += new System.EventHandler(this.BrowseButton1_Click);
            // 
            // Default_Paths
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(608, 341);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.SavePathsButton);
            this.Controls.Add(this.BrowseButton3);
            this.Controls.Add(this.BrowseButton2);
            this.Controls.Add(this.BrowseButton1);
            this.Controls.Add(this.UploadFilesPathText);
            this.Controls.Add(this.ExportReportPathText);
            this.Controls.Add(this.UploadImagesPathText);
            this.Controls.Add(this.ExportReportPathLabel);
            this.Controls.Add(this.UploadImagesPathLabel);
            this.Controls.Add(this.UploadFilesPathLabel);
            this.Name = "Default_Paths";
            this.Text = "Default_Paths";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label UploadFilesPathLabel;
        private System.Windows.Forms.Label UploadImagesPathLabel;
        private System.Windows.Forms.Label ExportReportPathLabel;
        private System.Windows.Forms.TextBox UploadImagesPathText;
        private System.Windows.Forms.TextBox ExportReportPathText;
        private System.Windows.Forms.TextBox UploadFilesPathText;
        private System.Windows.Forms.Button BrowseButton2;
        private System.Windows.Forms.Button BrowseButton3;
        private System.Windows.Forms.Button SavePathsButton;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.Button BrowseButton1;
    }
}