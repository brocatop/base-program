﻿namespace LogIn
{
    partial class Profiles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AssayTypeLabel = new System.Windows.Forms.Label();
            this.TempRangeLabel = new System.Windows.Forms.Label();
            this.AssayTypeTextBox = new System.Windows.Forms.TextBox();
            this.DeviationTextBox = new System.Windows.Forms.TextBox();
            this.TemperatureTextBox = new System.Windows.Forms.TextBox();
            this.PlusMinusLabel = new System.Windows.Forms.Label();
            this.CelciusLabel = new System.Windows.Forms.Label();
            this.SaveProfileButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.ProfileNameTextBox = new System.Windows.Forms.TextBox();
            this.ProfileNameLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // AssayTypeLabel
            // 
            this.AssayTypeLabel.AutoSize = true;
            this.AssayTypeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AssayTypeLabel.Location = new System.Drawing.Point(221, 58);
            this.AssayTypeLabel.Name = "AssayTypeLabel";
            this.AssayTypeLabel.Size = new System.Drawing.Size(123, 25);
            this.AssayTypeLabel.TabIndex = 0;
            this.AssayTypeLabel.Text = "Assay Type:";
            // 
            // TempRangeLabel
            // 
            this.TempRangeLabel.AutoSize = true;
            this.TempRangeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TempRangeLabel.Location = new System.Drawing.Point(413, 58);
            this.TempRangeLabel.Name = "TempRangeLabel";
            this.TempRangeLabel.Size = new System.Drawing.Size(358, 25);
            this.TempRangeLabel.TabIndex = 1;
            this.TempRangeLabel.Text = "Temperature Range for Cell Seperation:";
            // 
            // AssayTypeTextBox
            // 
            this.AssayTypeTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AssayTypeTextBox.Location = new System.Drawing.Point(226, 111);
            this.AssayTypeTextBox.Name = "AssayTypeTextBox";
            this.AssayTypeTextBox.Size = new System.Drawing.Size(100, 30);
            this.AssayTypeTextBox.TabIndex = 2;
            // 
            // DeviationTextBox
            // 
            this.DeviationTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeviationTextBox.Location = new System.Drawing.Point(445, 111);
            this.DeviationTextBox.Name = "DeviationTextBox";
            this.DeviationTextBox.Size = new System.Drawing.Size(100, 30);
            this.DeviationTextBox.TabIndex = 3;
            // 
            // TemperatureTextBox
            // 
            this.TemperatureTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TemperatureTextBox.Location = new System.Drawing.Point(594, 111);
            this.TemperatureTextBox.Name = "TemperatureTextBox";
            this.TemperatureTextBox.Size = new System.Drawing.Size(100, 30);
            this.TemperatureTextBox.TabIndex = 4;
            // 
            // PlusMinusLabel
            // 
            this.PlusMinusLabel.AutoSize = true;
            this.PlusMinusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PlusMinusLabel.Location = new System.Drawing.Point(551, 114);
            this.PlusMinusLabel.Name = "PlusMinusLabel";
            this.PlusMinusLabel.Size = new System.Drawing.Size(37, 25);
            this.PlusMinusLabel.TabIndex = 5;
            this.PlusMinusLabel.Text = "+/-";
            // 
            // CelciusLabel
            // 
            this.CelciusLabel.AutoSize = true;
            this.CelciusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CelciusLabel.Location = new System.Drawing.Point(700, 114);
            this.CelciusLabel.Name = "CelciusLabel";
            this.CelciusLabel.Size = new System.Drawing.Size(35, 25);
            this.CelciusLabel.TabIndex = 6;
            this.CelciusLabel.Text = "°C";
            // 
            // SaveProfileButton
            // 
            this.SaveProfileButton.Location = new System.Drawing.Point(236, 200);
            this.SaveProfileButton.Name = "SaveProfileButton";
            this.SaveProfileButton.Size = new System.Drawing.Size(124, 34);
            this.SaveProfileButton.TabIndex = 7;
            this.SaveProfileButton.Text = "Save Profile";
            this.SaveProfileButton.UseVisualStyleBackColor = true;
            this.SaveProfileButton.Click += new System.EventHandler(this.SaveProfileButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Location = new System.Drawing.Point(463, 200);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(124, 34);
            this.CancelButton.TabIndex = 8;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // ProfileNameTextBox
            // 
            this.ProfileNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProfileNameTextBox.Location = new System.Drawing.Point(44, 111);
            this.ProfileNameTextBox.Name = "ProfileNameTextBox";
            this.ProfileNameTextBox.Size = new System.Drawing.Size(124, 30);
            this.ProfileNameTextBox.TabIndex = 10;
            // 
            // ProfileNameLabel
            // 
            this.ProfileNameLabel.AutoSize = true;
            this.ProfileNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProfileNameLabel.Location = new System.Drawing.Point(39, 58);
            this.ProfileNameLabel.Name = "ProfileNameLabel";
            this.ProfileNameLabel.Size = new System.Drawing.Size(129, 25);
            this.ProfileNameLabel.TabIndex = 9;
            this.ProfileNameLabel.Text = "Profile Name:";
            // 
            // Profiles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 268);
            this.Controls.Add(this.ProfileNameTextBox);
            this.Controls.Add(this.ProfileNameLabel);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.SaveProfileButton);
            this.Controls.Add(this.CelciusLabel);
            this.Controls.Add(this.PlusMinusLabel);
            this.Controls.Add(this.TemperatureTextBox);
            this.Controls.Add(this.DeviationTextBox);
            this.Controls.Add(this.AssayTypeTextBox);
            this.Controls.Add(this.TempRangeLabel);
            this.Controls.Add(this.AssayTypeLabel);
            this.MaximumSize = new System.Drawing.Size(818, 315);
            this.MinimumSize = new System.Drawing.Size(818, 315);
            this.Name = "Profiles";
            this.Text = "Profiles";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label AssayTypeLabel;
        private System.Windows.Forms.Label TempRangeLabel;
        private System.Windows.Forms.TextBox AssayTypeTextBox;
        private System.Windows.Forms.TextBox DeviationTextBox;
        private System.Windows.Forms.TextBox TemperatureTextBox;
        private System.Windows.Forms.Label PlusMinusLabel;
        private System.Windows.Forms.Label CelciusLabel;
        private System.Windows.Forms.Button SaveProfileButton;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.TextBox ProfileNameTextBox;
        private System.Windows.Forms.Label ProfileNameLabel;
    }
}