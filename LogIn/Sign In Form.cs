﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Drawing;

namespace LogIn
{
    public partial class LogInForm : Form
    {

        public LogInForm()
        {
            InitializeComponent();
        }

        //Disables the sign in button and focuses the pointer to the username box
        private void LogInForm_Load(object sender, EventArgs e)
        {
            signInButton.Enabled = false;
            usernameBox.Focus();
        }


        //Signs the user into an account with the given credentials
        private void SignInButton_Click(object sender, EventArgs e)
        {
            string u = usernameBox.Text;
            string p = passwordBox.Text;
            string mc = @"Server=(local)\SQLEXPRESS; Database=Geno; integrated security=SSPI;";
            SqlConnection sc = new SqlConnection(mc);
            sc.Open();
            SqlCommand c = new SqlCommand("SELECT Username, Password from Users where Username= @u and Password =@p", sc );
            c.Parameters.AddWithValue("u", usernameBox.Text);
            c.Parameters.AddWithValue("p", passwordBox.Text);
            SqlDataAdapter da = new SqlDataAdapter(c);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                using (LandingPage lp = new LandingPage())
                {
                    this.Hide();
                    lp.ShowDialog(this);
                    this.Close();
                }
            }
            else if (string.IsNullOrWhiteSpace(usernameBox.Text) && string.IsNullOrWhiteSpace(passwordBox.Text))
            {
                MessageBox.Show("Please enter your username and password");
            }
            else if(string.IsNullOrWhiteSpace(usernameBox.Text))
            {
                MessageBox.Show("Please enter your username");
            }
            else if (string.IsNullOrWhiteSpace(passwordBox.Text))
            {
                MessageBox.Show("Please enter your password");
            }
            else
            {
                MessageBox.Show("Invalid Login please check username and password");
            }
            sc.Close();

        }

        //Sends the user to the create account window
        private void CreateAccountLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            using (RegisterForm rf = new RegisterForm())
            {
                rf.ShowDialog(this);
            }
        }

        //Sends the user to the forgot password window
        private void ForgotPasswordLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            using (ChangePassword cp = new ChangePassword())
            {
                cp.ShowDialog(this);
            }
        }

        //Disables the sign in button if the fields are empty. Enables the button when all of the fields have been filled
        private bool ValidateModuleDetails()
        {
            foreach (Control control in this.Controls)
                if (control.GetType() == typeof(TextBox))
                    if (!(String.IsNullOrEmpty(control.Text.Trim())) &&
                        !(String.IsNullOrWhiteSpace(control.Text.Trim())))
                    {
                        signInButton.Enabled = true;
                        return true;
                    }
                    else
                    {
                        signInButton.Enabled = false;
                        return false;
                    }
            return false;
        }

        //Utilize the validatemoduledetails for the textbox.
        private void UsernameBox_TextChanged(object sender, EventArgs e)
        {
            ValidateModuleDetails();
        }

        //Utilize the validatemoduledetails for the textbox. Hides the password with asterisks.
        private void PasswordBox_TextChanged(object sender, EventArgs e)
        {
            ValidateModuleDetails();
            passwordBox.PasswordChar = '*';
        }

        //If the user presses "ENTER" while in the textbox -- moves to the next box
        private void UsernameBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                passwordBox.Focus();
            }
        }

        //If the user presses "ENTER" while in the textbox -- clicks the sign in button
        private void PasswordBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                signInButton.PerformClick();
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            using (PreviewReportcs pr = new PreviewReportcs())
            {
                this.Hide();
                pr.ShowDialog(this);
                this.Close();
            }
        }

        /*
        protected override void OnResize(EventArgs e)
        {
            for (int ix = 0; ix < Controls.Count; ++ix)
            {
                int y1 = this.ClientSize.Height * ix / Controls.Count;
                int y2 = this.ClientSize.Height * (ix + 1) / Controls.Count;
                Controls[ix].Bounds = new Rectangle(0, y1, this.ClientSize.Width, y2 - y1);
            }
            base.OnResize(e);
        }
        protected override void OnLoad(EventArgs e)
        {
            OnResize(e);
            base.OnLoad(e);
        }
        */
    }
}
