﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace LogIn
{
    public partial class ChangePassword : Form
    {
        public ChangePassword()
        {
            InitializeComponent();
        }

        //Utilize the validatemoduledetails for the textbox.
        private void ChangePassword_Load(object sender, EventArgs e)
        {

        }

        //Utilize the validatemoduledetails for the textbox.
        private void CurrentPasswordBox_TextChanged(object sender, EventArgs e)
        {
            ValidateModuleDetails();
        }

        //Utilize the validatemoduledetails for the textbox. Hides the password with asterisks.
        private void NewPasswordBox_TextChanged(object sender, EventArgs e)
        {
            ValidateModuleDetails();
            NewPasswordBox.PasswordChar = '*';
        }

        //Utilize the validatemoduledetails for the textbox. Hides the password with asterisks.
        private void ConfirmNewPasswordBox_TextChanged(object sender, EventArgs e)
        {
            ValidateModuleDetails();
            ConfirmNewPasswordBox.PasswordChar = '*';
        }

        //If the password and confirm password box are equal, see if the old password existsm if it does, update the password
        private void CreateNewPasswordButton_Click(object sender, EventArgs e)
        {
            if(!ConfirmNewPasswordBox.Text.Equals(NewPasswordBox.Text))
            {
                MessageBox.Show("Fields do not match. Please try again");
            }
            else
            {
                string mc = @"Server=(local)\SQLEXPRESS; Database=Geno; integrated security=SSPI;";
                SqlConnection sc = new SqlConnection(mc);
                string op = CurrentPasswordBox.Text;
                string np = NewPasswordBox.Text;
                string cnp = ConfirmNewPasswordBox.Text;
                SqlCommand connectExist = new SqlCommand("SELECT COUNT(*) Password from Users where Password like @op;", sc);
                connectExist.Parameters.AddWithValue("@op", op);
                sc.Open();
                connectExist.ExecuteNonQuery();
                int passwordExists = (int)connectExist.ExecuteScalar();
                if (passwordExists > 0)
                {
                    if(VerifyPassword(cnp) == true)
                    {
                        SqlCommand connectUpdate = new SqlCommand("UPDATE Users SET Password = @np WHERE Password = @op;", sc);
                        connectUpdate.Parameters.AddWithValue("@np", np);
                        connectUpdate.Parameters.AddWithValue("@op", op);
                        connectUpdate.ExecuteNonQuery();
                        sc.Close();
                        MessageBox.Show("Your password has been successfully updated");
                    }
                    else
                    {
                        MessageBox.Show("Your password does not meet the minimum requirements");
                    }

                }
                else
                {
                    MessageBox.Show("The current password does not exist..");
                    sc.Close();
                }
            }
            
        }

        //Disables the create new password button if the fields are empty. Enables the button when all of the fields have been filled
        private bool ValidateModuleDetails()
        {
            foreach (Control control in this.Controls)
                if (control.GetType() == typeof(TextBox))
                    if (!(String.IsNullOrEmpty(control.Text.Trim())) &&
                        !(String.IsNullOrWhiteSpace(control.Text.Trim())))
                    {
                        CreateNewPasswordButton.Enabled = true;
                        return true;
                    }
                    else
                    {
                        CreateNewPasswordButton.Enabled = false;
                        return false;
                    }
            return false;
        }

        public static bool VerifyPassword(string password)
        {
            var hasNumber = new Regex(@"[0-9]+");
            var hasUpperChar = new Regex(@"[A-Z]+");
            var hasSpecialChars = new Regex(@"[!@#$%^&*()_+=\[{\]}:<>|./?,-]");
            var hasMinimum8Chars = new Regex(@".{8,}");

            bool isValidated = hasNumber.IsMatch(password) && hasUpperChar.IsMatch(password) && hasMinimum8Chars.IsMatch(password) && hasSpecialChars.IsMatch(password);
            return isValidated;
        }

    }
}
