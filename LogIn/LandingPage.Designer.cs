﻿namespace LogIn
{
    partial class LandingPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ClearBatchButton = new System.Windows.Forms.Button();
            this.PreviewReportButton = new System.Windows.Forms.Button();
            this.UploadFilesButton = new System.Windows.Forms.Button();
            this.FilesUploadedLabel = new System.Windows.Forms.Label();
            this.AssayTypesLabel = new System.Windows.Forms.Label();
            this.TargetedTransgeneLabel = new System.Windows.Forms.Label();
            this.SeperatedButton = new System.Windows.Forms.RadioButton();
            this.MultiplexedButton = new System.Windows.Forms.RadioButton();
            this.QPCRButton = new System.Windows.Forms.RadioButton();
            this.EndpointButton = new System.Windows.Forms.RadioButton();
            this.GelButton = new System.Windows.Forms.RadioButton();
            this.TargetedButton = new System.Windows.Forms.RadioButton();
            this.TransgeneButton = new System.Windows.Forms.RadioButton();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.defaultPathsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createReportProfilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.ClearFirstRowLabel = new System.Windows.Forms.LinkLabel();
            this.ClearSecondRowLink = new System.Windows.Forms.LinkLabel();
            this.ClearThirdRowLink = new System.Windows.Forms.LinkLabel();
            this.ClearFourthRowLink = new System.Windows.Forms.LinkLabel();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // ClearBatchButton
            // 
            this.ClearBatchButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.ClearBatchButton.Location = new System.Drawing.Point(529, 260);
            this.ClearBatchButton.Name = "ClearBatchButton";
            this.ClearBatchButton.Size = new System.Drawing.Size(159, 37);
            this.ClearBatchButton.TabIndex = 0;
            this.ClearBatchButton.Text = "Clear Batch";
            this.ClearBatchButton.UseVisualStyleBackColor = true;
            this.ClearBatchButton.Click += new System.EventHandler(this.ClearBatchButton_Click);
            // 
            // PreviewReportButton
            // 
            this.PreviewReportButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.PreviewReportButton.Location = new System.Drawing.Point(529, 363);
            this.PreviewReportButton.Name = "PreviewReportButton";
            this.PreviewReportButton.Size = new System.Drawing.Size(159, 37);
            this.PreviewReportButton.TabIndex = 1;
            this.PreviewReportButton.Text = "Preview Report";
            this.PreviewReportButton.UseVisualStyleBackColor = true;
            this.PreviewReportButton.Click += new System.EventHandler(this.PreviewReportButton_Click);
            // 
            // UploadFilesButton
            // 
            this.UploadFilesButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.UploadFilesButton.Location = new System.Drawing.Point(529, 312);
            this.UploadFilesButton.Name = "UploadFilesButton";
            this.UploadFilesButton.Size = new System.Drawing.Size(159, 37);
            this.UploadFilesButton.TabIndex = 2;
            this.UploadFilesButton.Text = "Upload FIles...";
            this.UploadFilesButton.UseVisualStyleBackColor = true;
            this.UploadFilesButton.Click += new System.EventHandler(this.UploadFilesButton_Click);
            // 
            // FilesUploadedLabel
            // 
            this.FilesUploadedLabel.AutoSize = true;
            this.FilesUploadedLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.FilesUploadedLabel.Location = new System.Drawing.Point(12, 230);
            this.FilesUploadedLabel.Name = "FilesUploadedLabel";
            this.FilesUploadedLabel.Size = new System.Drawing.Size(148, 25);
            this.FilesUploadedLabel.TabIndex = 3;
            this.FilesUploadedLabel.Text = "Files Uploaded:";
            // 
            // AssayTypesLabel
            // 
            this.AssayTypesLabel.AutoSize = true;
            this.AssayTypesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AssayTypesLabel.Location = new System.Drawing.Point(12, 44);
            this.AssayTypesLabel.Name = "AssayTypesLabel";
            this.AssayTypesLabel.Size = new System.Drawing.Size(133, 25);
            this.AssayTypesLabel.TabIndex = 4;
            this.AssayTypesLabel.Text = "Assay Types:";
            // 
            // TargetedTransgeneLabel
            // 
            this.TargetedTransgeneLabel.AutoSize = true;
            this.TargetedTransgeneLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TargetedTransgeneLabel.Location = new System.Drawing.Point(469, 44);
            this.TargetedTransgeneLabel.Name = "TargetedTransgeneLabel";
            this.TargetedTransgeneLabel.Size = new System.Drawing.Size(219, 25);
            this.TargetedTransgeneLabel.TabIndex = 5;
            this.TargetedTransgeneLabel.Text = "Targeted or Transgene:";
            // 
            // SeperatedButton
            // 
            this.SeperatedButton.AutoSize = true;
            this.SeperatedButton.Location = new System.Drawing.Point(12, 88);
            this.SeperatedButton.Name = "SeperatedButton";
            this.SeperatedButton.Size = new System.Drawing.Size(95, 21);
            this.SeperatedButton.TabIndex = 6;
            this.SeperatedButton.TabStop = true;
            this.SeperatedButton.Text = "Seperated";
            this.SeperatedButton.UseVisualStyleBackColor = true;
            // 
            // MultiplexedButton
            // 
            this.MultiplexedButton.AutoSize = true;
            this.MultiplexedButton.Location = new System.Drawing.Point(12, 115);
            this.MultiplexedButton.Name = "MultiplexedButton";
            this.MultiplexedButton.Size = new System.Drawing.Size(99, 21);
            this.MultiplexedButton.TabIndex = 7;
            this.MultiplexedButton.TabStop = true;
            this.MultiplexedButton.Text = "Multiplexed";
            this.MultiplexedButton.UseVisualStyleBackColor = true;
            // 
            // QPCRButton
            // 
            this.QPCRButton.AutoSize = true;
            this.QPCRButton.Location = new System.Drawing.Point(12, 142);
            this.QPCRButton.Name = "QPCRButton";
            this.QPCRButton.Size = new System.Drawing.Size(69, 21);
            this.QPCRButton.TabIndex = 8;
            this.QPCRButton.TabStop = true;
            this.QPCRButton.Text = "qPCR:";
            this.QPCRButton.UseVisualStyleBackColor = true;
            // 
            // EndpointButton
            // 
            this.EndpointButton.AutoSize = true;
            this.EndpointButton.Location = new System.Drawing.Point(12, 169);
            this.EndpointButton.Name = "EndpointButton";
            this.EndpointButton.Size = new System.Drawing.Size(85, 21);
            this.EndpointButton.TabIndex = 9;
            this.EndpointButton.TabStop = true;
            this.EndpointButton.Text = "Endpoint";
            this.EndpointButton.UseVisualStyleBackColor = true;
            // 
            // GelButton
            // 
            this.GelButton.AutoSize = true;
            this.GelButton.Location = new System.Drawing.Point(12, 196);
            this.GelButton.Name = "GelButton";
            this.GelButton.Size = new System.Drawing.Size(51, 21);
            this.GelButton.TabIndex = 10;
            this.GelButton.TabStop = true;
            this.GelButton.Text = "Gel";
            this.GelButton.UseVisualStyleBackColor = true;
            // 
            // TargetedButton
            // 
            this.TargetedButton.AutoSize = true;
            this.TargetedButton.Location = new System.Drawing.Point(529, 88);
            this.TargetedButton.Name = "TargetedButton";
            this.TargetedButton.Size = new System.Drawing.Size(87, 21);
            this.TargetedButton.TabIndex = 11;
            this.TargetedButton.TabStop = true;
            this.TargetedButton.Text = "Targeted";
            this.TargetedButton.UseVisualStyleBackColor = true;
            // 
            // TransgeneButton
            // 
            this.TransgeneButton.AutoSize = true;
            this.TransgeneButton.Location = new System.Drawing.Point(529, 115);
            this.TransgeneButton.Name = "TransgeneButton";
            this.TransgeneButton.Size = new System.Drawing.Size(98, 21);
            this.TransgeneButton.TabIndex = 12;
            this.TransgeneButton.TabStop = true;
            this.TransgeneButton.Text = "Transgene";
            this.TransgeneButton.UseVisualStyleBackColor = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(700, 28);
            this.menuStrip1.TabIndex = 13;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.defaultPathsToolStripMenuItem,
            this.createReportProfilesToolStripMenuItem});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(58, 24);
            this.menuToolStripMenuItem.Text = "Menu";
            // 
            // defaultPathsToolStripMenuItem
            // 
            this.defaultPathsToolStripMenuItem.Name = "defaultPathsToolStripMenuItem";
            this.defaultPathsToolStripMenuItem.Size = new System.Drawing.Size(229, 26);
            this.defaultPathsToolStripMenuItem.Text = "Default Paths";
            this.defaultPathsToolStripMenuItem.Click += new System.EventHandler(this.DefaultPathsToolStripMenuItem_Click);
            // 
            // createReportProfilesToolStripMenuItem
            // 
            this.createReportProfilesToolStripMenuItem.Name = "createReportProfilesToolStripMenuItem";
            this.createReportProfilesToolStripMenuItem.Size = new System.Drawing.Size(229, 26);
            this.createReportProfilesToolStripMenuItem.Text = "Create Report Profiles";
            this.createReportProfilesToolStripMenuItem.Click += new System.EventHandler(this.CreateReportProfilesToolStripMenuItem_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(17, 281);
            this.dataGridView1.MaximumSize = new System.Drawing.Size(307, 125);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(307, 125);
            this.dataGridView1.TabIndex = 14;
            // 
            // ClearFirstRowLabel
            // 
            this.ClearFirstRowLabel.AutoSize = true;
            this.ClearFirstRowLabel.Location = new System.Drawing.Point(364, 312);
            this.ClearFirstRowLabel.Name = "ClearFirstRowLabel";
            this.ClearFirstRowLabel.Size = new System.Drawing.Size(103, 17);
            this.ClearFirstRowLabel.TabIndex = 15;
            this.ClearFirstRowLabel.TabStop = true;
            this.ClearFirstRowLabel.Text = "Clear First Row";
            this.ClearFirstRowLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.ClearFirstRowLabel_LinkClicked);
            // 
            // ClearSecondRowLink
            // 
            this.ClearSecondRowLink.AutoSize = true;
            this.ClearSecondRowLink.Location = new System.Drawing.Point(364, 332);
            this.ClearSecondRowLink.Name = "ClearSecondRowLink";
            this.ClearSecondRowLink.Size = new System.Drawing.Size(124, 17);
            this.ClearSecondRowLink.TabIndex = 16;
            this.ClearSecondRowLink.TabStop = true;
            this.ClearSecondRowLink.Text = "Clear Second Row";
            this.ClearSecondRowLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.ClearSecondRowLink_LinkClicked);
            // 
            // ClearThirdRowLink
            // 
            this.ClearThirdRowLink.AutoSize = true;
            this.ClearThirdRowLink.Location = new System.Drawing.Point(364, 353);
            this.ClearThirdRowLink.Name = "ClearThirdRowLink";
            this.ClearThirdRowLink.Size = new System.Drawing.Size(109, 17);
            this.ClearThirdRowLink.TabIndex = 17;
            this.ClearThirdRowLink.TabStop = true;
            this.ClearThirdRowLink.Text = "Clear Third Row";
            this.ClearThirdRowLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.ClearThirdRowLink_LinkClicked);
            // 
            // ClearFourthRowLink
            // 
            this.ClearFourthRowLink.AutoSize = true;
            this.ClearFourthRowLink.Location = new System.Drawing.Point(364, 375);
            this.ClearFourthRowLink.Name = "ClearFourthRowLink";
            this.ClearFourthRowLink.Size = new System.Drawing.Size(117, 17);
            this.ClearFourthRowLink.TabIndex = 18;
            this.ClearFourthRowLink.TabStop = true;
            this.ClearFourthRowLink.Text = "Clear Fourth Row";
            this.ClearFourthRowLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.ClearFourthRowLink_LinkClicked);
            // 
            // LandingPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(700, 422);
            this.Controls.Add(this.ClearFourthRowLink);
            this.Controls.Add(this.ClearThirdRowLink);
            this.Controls.Add(this.ClearSecondRowLink);
            this.Controls.Add(this.ClearFirstRowLabel);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.TransgeneButton);
            this.Controls.Add(this.TargetedButton);
            this.Controls.Add(this.GelButton);
            this.Controls.Add(this.EndpointButton);
            this.Controls.Add(this.QPCRButton);
            this.Controls.Add(this.MultiplexedButton);
            this.Controls.Add(this.SeperatedButton);
            this.Controls.Add(this.TargetedTransgeneLabel);
            this.Controls.Add(this.AssayTypesLabel);
            this.Controls.Add(this.FilesUploadedLabel);
            this.Controls.Add(this.UploadFilesButton);
            this.Controls.Add(this.PreviewReportButton);
            this.Controls.Add(this.ClearBatchButton);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximumSize = new System.Drawing.Size(718, 469);
            this.MinimumSize = new System.Drawing.Size(718, 469);
            this.Name = "LandingPage";
            this.Text = "LandingPage";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ClearBatchButton;
        private System.Windows.Forms.Button PreviewReportButton;
        private System.Windows.Forms.Button UploadFilesButton;
        private System.Windows.Forms.Label FilesUploadedLabel;
        private System.Windows.Forms.Label AssayTypesLabel;
        private System.Windows.Forms.Label TargetedTransgeneLabel;
        private System.Windows.Forms.RadioButton SeperatedButton;
        private System.Windows.Forms.RadioButton MultiplexedButton;
        private System.Windows.Forms.RadioButton QPCRButton;
        private System.Windows.Forms.RadioButton EndpointButton;
        private System.Windows.Forms.RadioButton GelButton;
        private System.Windows.Forms.RadioButton TargetedButton;
        private System.Windows.Forms.RadioButton TransgeneButton;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem defaultPathsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createReportProfilesToolStripMenuItem;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.LinkLabel ClearFirstRowLabel;
        private System.Windows.Forms.LinkLabel ClearSecondRowLink;
        private System.Windows.Forms.LinkLabel ClearThirdRowLink;
        private System.Windows.Forms.LinkLabel ClearFourthRowLink;
    }
}