﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.IO;
using System.Drawing;

namespace LogIn
{
    public partial class UploadFiles : Form
    {

        public UploadFiles()
        {
            InitializeComponent();
            PopulateComboBox();
            MinimizeBox = false;
            MaximizeBox = false;
            ComboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
            FilesUploadedLabel.Hide();
            FillPaths();
        }


        public static bool InsertData(List<Data> Data)
        {
            int i = 2;
            bool succesful;
            using (SqlConnection conn = new SqlConnection("server=(local)\\SQLEXPRESS; database=GTCA; integrated Security=SSPI"))
            {
                using (SqlCommand cmd = new SqlCommand("INSERT INTO dbo.FileRows (File_dataID, SampleName, temp1, area1, temp2, area2) VALUES (@id, @pos, @temp1, @area1, @temp2, @area2)", conn))
                {
                    conn.Open();
                    foreach (Data items in Data)
                    {
                        cmd.Parameters.Add("@id", SqlDbType.Int).Value = i; i++;
                        cmd.Parameters.Add("@pos", SqlDbType.VarChar, 50).Value = items.pos;
                        cmd.Parameters.Add("@temp1", SqlDbType.Float, 24).Value = items.Tm1;
                        cmd.Parameters.Add("@area1", SqlDbType.Float, 24).Value = items.Area1;
                        cmd.Parameters.Add("@temp2", SqlDbType.Float, 24).Value = items.Tm2;
                        cmd.Parameters.Add("@area2", SqlDbType.Float, 24).Value = items.Area2;
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }
                    conn.Close();
                    succesful = true;
                }
            }
            return succesful;
        }

        public static List<Data> ReadFile(string filepath)
        {
            string line;
            List<Data> values = new List<Data>();
            using (StreamReader file = new StreamReader(filepath))
            {
                file.ReadLine();
                file.ReadLine();
                while ((line = file.ReadLine()) != null)
                {
                    String pos = line.Split('\t')[2];
                    float Tm1 = 0.00f;
                    float Area1 = 0.00f;
                    float Tm2 = 0.00f;
                    float Area2 = 0.00f;
                    if (float.TryParse(line.Split('\t')[4], out float r))
                    {
                        Tm1 = float.Parse(line.Split('\t')[4]);
                    }
                    if (float.TryParse(line.Split('\t')[5], out r))
                    {
                        Area1 = float.Parse(line.Split('\t')[5]);
                    }
                    if (float.TryParse(line.Split('\t')[6], out r))
                    {
                        Tm2 = float.Parse(line.Split('\t')[6]);
                    }
                    if (float.TryParse(line.Split('\t')[7], out r))
                    {
                        Area2 = float.Parse(line.Split('\t')[7]);
                    }
                    Data Row = new Data(pos, Tm1, Area1, Tm2, Area2);
                    values.Add(Row);
                }
            }
            return values;
        }

        private void UploadButton_Click(object sender, EventArgs e)
        {
            Image i = Image.FromFile(Path.GetFileName(ImageUploadText.Text));
            byte[] data = ImageToByteArray(i);
            
            //Inserts the file to the database
            InsertData(ReadFile(FileUploadText.Text));
        }

        private void UploadImageToDB()
        {

        }

        private void BrowseButton1_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.InitialDirectory = FileUploadText.Text;
            ofd.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            DialogResult dr = ofd.ShowDialog();
            if(dr == DialogResult.OK)
            {
                FileUploadText.Text = ofd.FileName;
            }
        }

        private void BrowseButton2_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.InitialDirectory = ImageUploadText.Text;
            ofd.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            DialogResult dr = ofd.ShowDialog();
            if (dr == DialogResult.OK)
            {
                ImageUploadText.Text = ofd.FileName;
            }
        }

        private void ComboBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            string ID = ComboBox1.SelectedValue.ToString();
            if (ComboBox1.SelectedIndex == ComboBox1.Items.Count - 1)
            {
                using (Profiles p = new Profiles())
                {
                    p.ShowDialog();
                }
            }
            PopulateComboBox();
        }

        private void FillPaths()
        {
            string mc = @"Server=(local)\SQLEXPRESS; Database=Geno; integrated security=SSPI;";
            using (SqlConnection sc = new SqlConnection(mc))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM UserDefaults ", sc))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.Connection = sc;
                        sda.SelectCommand = cmd;
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            FileUploadText.Text = dt.Rows[0]["TextPath"].ToString();
                            ImageUploadText.Text = dt.Rows[0]["ImagePath"].ToString();
                        }
                    }
                }
            }
        }

        private void PopulateComboBox()
        {
            string mc = @"Server=(local)\SQLEXPRESS; Database=Geno; integrated security=SSPI;";
            using (SqlConnection sc = new SqlConnection(mc))
            {
                SqlDataAdapter da = new SqlDataAdapter("SELECT ProfileName FROM PROFILES", sc);
                sc.Open();
                DataTable dt = new DataTable();
                da.Fill(dt);
                ComboBox1.DisplayMember = "ProfileName";
                ComboBox1.ValueMember = "ProfileName";
                DataRow CP = dt.NewRow();
                CP["ProfileName"] = "Create New Profile";
                dt.Rows.InsertAt(CP, dt.Rows.Count);             
                ComboBox1.DataSource = dt;
                sc.Close();
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private byte[] ImageToByteArray(Image x)
        {
            MemoryStream ms = new MemoryStream();
            try
            {
                x.Save(ms, x.RawFormat);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return ms.ToArray();
        }

        private byte[] ImageToByteArray2(Image x)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                try
                {
                    x.Save(ms, x.RawFormat);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                return ms.ToArray();
            }
        }
    }
}
