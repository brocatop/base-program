﻿namespace LogIn
{
    partial class ChangePassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OldPasswordLabel = new System.Windows.Forms.Label();
            this.NewPasswordLabel = new System.Windows.Forms.Label();
            this.ConfirmNewPassword = new System.Windows.Forms.Label();
            this.ConfirmNewPasswordBox = new System.Windows.Forms.TextBox();
            this.NewPasswordBox = new System.Windows.Forms.TextBox();
            this.CurrentPasswordBox = new System.Windows.Forms.TextBox();
            this.CreateNewPasswordButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // OldPasswordLabel
            // 
            this.OldPasswordLabel.AutoSize = true;
            this.OldPasswordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OldPasswordLabel.Location = new System.Drawing.Point(56, 109);
            this.OldPasswordLabel.Name = "OldPasswordLabel";
            this.OldPasswordLabel.Size = new System.Drawing.Size(166, 20);
            this.OldPasswordLabel.TabIndex = 0;
            this.OldPasswordLabel.Text = "Current Password:";
            // 
            // NewPasswordLabel
            // 
            this.NewPasswordLabel.AutoSize = true;
            this.NewPasswordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NewPasswordLabel.Location = new System.Drawing.Point(56, 158);
            this.NewPasswordLabel.Name = "NewPasswordLabel";
            this.NewPasswordLabel.Size = new System.Drawing.Size(139, 20);
            this.NewPasswordLabel.TabIndex = 1;
            this.NewPasswordLabel.Text = "New Password:";
            // 
            // ConfirmNewPassword
            // 
            this.ConfirmNewPassword.AutoSize = true;
            this.ConfirmNewPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConfirmNewPassword.Location = new System.Drawing.Point(56, 205);
            this.ConfirmNewPassword.Name = "ConfirmNewPassword";
            this.ConfirmNewPassword.Size = new System.Drawing.Size(211, 20);
            this.ConfirmNewPassword.TabIndex = 2;
            this.ConfirmNewPassword.Text = "Confirm New Password:";
            // 
            // ConfirmNewPasswordBox
            // 
            this.ConfirmNewPasswordBox.Location = new System.Drawing.Point(282, 205);
            this.ConfirmNewPasswordBox.Name = "ConfirmNewPasswordBox";
            this.ConfirmNewPasswordBox.Size = new System.Drawing.Size(306, 22);
            this.ConfirmNewPasswordBox.TabIndex = 4;
            this.ConfirmNewPasswordBox.TextChanged += new System.EventHandler(this.ConfirmNewPasswordBox_TextChanged);
            // 
            // NewPasswordBox
            // 
            this.NewPasswordBox.Location = new System.Drawing.Point(282, 158);
            this.NewPasswordBox.Name = "NewPasswordBox";
            this.NewPasswordBox.Size = new System.Drawing.Size(306, 22);
            this.NewPasswordBox.TabIndex = 5;
            this.NewPasswordBox.TextChanged += new System.EventHandler(this.NewPasswordBox_TextChanged);
            // 
            // CurrentPasswordBox
            // 
            this.CurrentPasswordBox.Location = new System.Drawing.Point(282, 109);
            this.CurrentPasswordBox.Name = "CurrentPasswordBox";
            this.CurrentPasswordBox.Size = new System.Drawing.Size(306, 22);
            this.CurrentPasswordBox.TabIndex = 6;
            this.CurrentPasswordBox.TextChanged += new System.EventHandler(this.CurrentPasswordBox_TextChanged);
            // 
            // CreateNewPasswordButton
            // 
            this.CreateNewPasswordButton.Location = new System.Drawing.Point(303, 368);
            this.CreateNewPasswordButton.Name = "CreateNewPasswordButton";
            this.CreateNewPasswordButton.Size = new System.Drawing.Size(178, 40);
            this.CreateNewPasswordButton.TabIndex = 7;
            this.CreateNewPasswordButton.Text = "Create New Password";
            this.CreateNewPasswordButton.UseVisualStyleBackColor = true;
            this.CreateNewPasswordButton.Click += new System.EventHandler(this.CreateNewPasswordButton_Click);
            // 
            // ChangePassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.CreateNewPasswordButton);
            this.Controls.Add(this.CurrentPasswordBox);
            this.Controls.Add(this.NewPasswordBox);
            this.Controls.Add(this.ConfirmNewPasswordBox);
            this.Controls.Add(this.ConfirmNewPassword);
            this.Controls.Add(this.NewPasswordLabel);
            this.Controls.Add(this.OldPasswordLabel);
            this.Name = "ChangePassword";
            this.Text = "ChangePassword";
            this.Load += new System.EventHandler(this.ChangePassword_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label OldPasswordLabel;
        private System.Windows.Forms.Label NewPasswordLabel;
        private System.Windows.Forms.Label ConfirmNewPassword;
        private System.Windows.Forms.TextBox ConfirmNewPasswordBox;
        private System.Windows.Forms.TextBox NewPasswordBox;
        private System.Windows.Forms.TextBox CurrentPasswordBox;
        private System.Windows.Forms.Button CreateNewPasswordButton;
    }
}