﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;

namespace LogIn
{
    public partial class Default_Paths : Form
    {
        public Default_Paths()
        {
            InitializeComponent();
            MinimizeBox = false;
            MaximizeBox = false;
            FillPaths();
        }

        //Takes the paths in the textboxes and uploads them to the database
        public void SavePathsButton_Click(object sender, EventArgs e)
        {
            if(Directory.Exists(UploadFilesPathText.Text) || string.IsNullOrWhiteSpace(UploadFilesPathText.Text))
            {
                if(Directory.Exists(UploadImagesPathText.Text) || string.IsNullOrWhiteSpace(UploadImagesPathText.Text))
                {
                    if(Directory.Exists(ExportReportPathText.Text) || string.IsNullOrWhiteSpace(ExportReportPathText.Text))
                    {
                        string mc = @"Server=(local)\SQLEXPRESS; Database=Geno; integrated security=SSPI;";
                        using (SqlConnection sc = new SqlConnection(mc))
                        {

                            using (SqlCommand c = new SqlCommand("IF NOT EXISTS (SELECT * FROM UserDefaults WHERE ID = 1) BEGIN INSERT INTO UserDefaults(TextPath, ImagePath, ExportPath)" +
                                " VALUES(@tp, @ip, @ep) END ELSE BEGIN  UPDATE UserDefaults SET TextPath = @tp, ImagePath = @ip, ExportPath = @ep END;", sc))
                            {
                                sc.Open();
                                c.Parameters.AddWithValue("@tp", UploadFilesPathText.Text.TrimStart());
                                c.Parameters.AddWithValue("@ip", UploadImagesPathText.Text.TrimStart());
                                c.Parameters.AddWithValue("@ep", ExportReportPathText.Text.TrimStart());
                                c.ExecuteNonQuery();
                                sc.Close();
                            }                             
                        }
                    }
                    else
                    {
                        MessageBox.Show("The paths entered are not valid. Please enter a valid directory");
                    }
                }
                else
                {
                    MessageBox.Show("The paths entered are not valid. Please enter a valid directory");
                }              
            }
            else
            {
                MessageBox.Show("The paths entered are not valid. Please enter a valid directory");
            }  
        }

        //Browses to the desired folder path
        private void BrowseButton1_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog fd = new FolderBrowserDialog())
            {
                fd.ShowDialog();
                if(fd.ShowDialog() == DialogResult.OK)
                {
                    UploadFilesPathText.Text = fd.SelectedPath;
                }
            }
        }

        //Browses to the desired folder path
        private void BrowseButton2_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog fd = new FolderBrowserDialog())
            {
                fd.ShowDialog();
                if (fd.ShowDialog() == DialogResult.OK)
                {
                    UploadImagesPathText.Text = fd.SelectedPath;
                }
            }
        }

        //Browses to the desired folder path
        private void BrowseButton3_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog fd = new FolderBrowserDialog())
            {
                fd.ShowDialog();
                if (fd.ShowDialog() == DialogResult.OK)
                {
                    ExportReportPathText.Text = fd.SelectedPath;
                }
            }
        }

        //Closes the default paths window
        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Fills the textboxes with the default paths from the database
        private void FillPaths()
        {
            string mc = @"Server=(local)\SQLEXPRESS; Database=Geno; integrated security=SSPI;";
            using (SqlConnection sc = new SqlConnection(mc))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM UserDefaults ", sc))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.Connection = sc;
                        sda.SelectCommand = cmd;
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            UploadFilesPathText.Text = dt.Rows[0]["TextPath"].ToString();
                            UploadImagesPathText.Text = dt.Rows[0]["ImagePath"].ToString();
                            ExportReportPathText.Text = dt.Rows[0]["ExportPath"].ToString();
                        }
                    }
                }             
            }            
        }
    }
}
