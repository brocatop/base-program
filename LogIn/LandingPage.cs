﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace LogIn
{
    public partial class LandingPage : Form
    {
        public LandingPage()
        {
            ConstantLoad();
            dataGridView1.AllowUserToAddRows = false;
            QPCRButton.Enabled = false;
            EndpointButton.Enabled = false;
            GelButton.Enabled = false;
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
        }

        //Opens another form
        private void PreviewReportButton_Click(object sender, EventArgs e)
        {
            using (PreviewReportcs pr = new PreviewReportcs())
            {
                pr.ShowDialog();
            }
        }

        //Opens another form
        private void UploadFilesButton_Click(object sender, EventArgs e)
        {
            using (UploadFiles uf = new UploadFiles())
            {
                uf.ShowDialog();
            }
        }

        //Opens another form
        private void DefaultPathsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (Default_Paths dp = new Default_Paths())
            {
                dp.ShowDialog();
            }
        }

        //Opens another form
        private void CreateReportProfilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (Profiles p = new Profiles())
            {
                p.ShowDialog();
            }
        }

        //Clears the files in the directory...Will clear FIles in the File Table. Then it updates the landing page
        private void ClearBatchButton_Click(object sender, EventArgs e)
        {
            DirectoryInfo di = new DirectoryInfo("C:\\Users\\patrick.brocato\\source\\repos\\LogIn\\LogIn\\Upload Files");
            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }
            PopulateUploadedFiles();
        }

        //Creates a list of uploaded files and displays each file
        public void PopulateUploadedFiles()
        {
            string[] fileArray = Directory.GetFiles(@"C:\\Users\\patrick.brocato\\source\\repos\\LogIn\\LogIn\\Upload Files");
            DataTable dt = new DataTable();
            dt.Columns.Add("File");
            dt.Columns.Add("Image");
            dt.Rows.Add();
            dt.Rows.Add();
            dt.Rows.Add();
            dt.Rows.Add();
            int arrayIndex = 0;
            int imageIndex = 0;
            int fileIndex = 0;
            string name;
            foreach (string s in fileArray)
            {
                name = Path.GetFileName(fileArray[arrayIndex]);
                if (name.Contains(".jpg"))
                {
                    if (string.IsNullOrEmpty(dt.Rows[imageIndex]["Image"].ToString()))
                    {
                        dt.Rows[imageIndex]["Image"] = name;
                        arrayIndex++;
                        imageIndex++;
                    }
                    else
                    {
                        dt.Rows[imageIndex + 1]["Image"] = name;
                        arrayIndex++;
                        imageIndex++;
                    }
                }
                else if (name.Contains(".txt"))
                {
                    if(string.IsNullOrEmpty(dt.Rows[fileIndex]["File"].ToString()))
                    {
                        dt.Rows[fileIndex]["File"] = name;
                        arrayIndex++;
                        fileIndex++;
                    }
                    else
                    {
                        dt.Rows[fileIndex + 1]["File"] = name;
                        arrayIndex++;
                        fileIndex++;
                    }                   
                }
                                
            }
            dataGridView1.DataSource = dt;
        }

        //For the sake of simplicity, these are all of the formatting details into a method
        private void ConstantLoad()
        {
            InitializeComponent();
            this.MaximizeBox = false;
            menuToolStripMenuItem.Anchor = AnchorStyles.Left | AnchorStyles.Top;
            UploadFilesButton.Anchor = AnchorStyles.Right;
            ClearBatchButton.Anchor = AnchorStyles.Right;
            PreviewReportButton.Anchor = AnchorStyles.Right;
            AssayTypesLabel.Anchor = AnchorStyles.Left;
            SeperatedButton.Anchor = AnchorStyles.Left;
            MultiplexedButton.Anchor = AnchorStyles.Left;
            QPCRButton.Anchor = AnchorStyles.Left;
            EndpointButton.Anchor = AnchorStyles.Left;
            GelButton.Anchor = AnchorStyles.Left;
            TargetedTransgeneLabel.Anchor = AnchorStyles.Right;
            TargetedButton.Anchor = AnchorStyles.Right;
            TransgeneButton.Anchor = AnchorStyles.Right;
            FilesUploadedLabel.Anchor = AnchorStyles.Left;
            ClearFirstRowLabel.Anchor = AnchorStyles.Bottom;
            ClearSecondRowLink.Anchor = AnchorStyles.Bottom;
            ClearThirdRowLink.Anchor = AnchorStyles.Bottom;
            ClearFourthRowLink.Anchor = AnchorStyles.Bottom;
            PopulateUploadedFiles();
            DataGridViewElementStates states = DataGridViewElementStates.None;
            dataGridView1.ScrollBars = ScrollBars.None;
            var totalHeight = dataGridView1.Rows.GetRowsHeight(states) + dataGridView1.ColumnHeadersHeight;
            totalHeight += dataGridView1.Rows.Count;  // a correction I need
            var totalWidth = dataGridView1.Columns.GetColumnsWidth(states) + dataGridView1.RowHeadersWidth;
            dataGridView1.ClientSize = new Size(totalWidth, totalHeight);
            dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;

        }

        //Deletes the files in the first row in the directory...Will clear specified files in the database
        private void ClearFirstRowLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DirectoryInfo di = new DirectoryInfo("C:\\Users\\patrick.brocato\\source\\repos\\LogIn\\LogIn\\Upload Files");
            foreach (FileInfo file in di.GetFiles())
            {
                if(file.Name == dataGridView1.Rows[0].Cells["File"].Value.ToString() || file.Name == dataGridView1.Rows[0].Cells["Image"].Value.ToString())
                {
                    file.Delete();
                }
            }
            PopulateUploadedFiles();
        }
        //Deletes the files in the second row in the directory...Will clear specified files in the database

        private void ClearSecondRowLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DirectoryInfo di = new DirectoryInfo("C:\\Users\\patrick.brocato\\source\\repos\\LogIn\\LogIn\\Upload Files");
            foreach (FileInfo file in di.GetFiles())
            {
                if (file.Name == dataGridView1.Rows[1].Cells["File"].Value.ToString() || file.Name == dataGridView1.Rows[1].Cells["Image"].Value.ToString())
                {
                    file.Delete();
                }
            }
            PopulateUploadedFiles();
        }

        //Deletes the files in the third row in the directory...Will clear specified files in the database
        private void ClearThirdRowLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DirectoryInfo di = new DirectoryInfo("C:\\Users\\patrick.brocato\\source\\repos\\LogIn\\LogIn\\Upload Files");
            foreach (FileInfo file in di.GetFiles())
            {
                if (file.Name == dataGridView1.Rows[2].Cells["File"].Value.ToString() || file.Name == dataGridView1.Rows[2].Cells["Image"].Value.ToString())
                {
                    file.Delete();
                }
            }
            PopulateUploadedFiles();
        }

        //Deletes the files in the fourth row in the directory...Will clear specified files in the database
        private void ClearFourthRowLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DirectoryInfo di = new DirectoryInfo("C:\\Users\\patrick.brocato\\source\\repos\\LogIn\\LogIn\\Upload Files");
            foreach (FileInfo file in di.GetFiles())
            {
                if (file.Name == dataGridView1.Rows[3].Cells["File"].Value.ToString() || file.Name == dataGridView1.Rows[3].Cells["Image"].Value.ToString())
                {
                    file.Delete();
                }
            }
            PopulateUploadedFiles();
        }
    }
}
