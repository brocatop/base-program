﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LogIn
{
    class User
    {

        public string Username;
        public string Password;


        public User(string Username, string Password)
        {
            this.Username = Username;
            this.Password = Password;
        }

        private bool StringValidator(string i)
        {
            string pattern = "[^a-zA-Z]";
            if (Regex.IsMatch(i, pattern))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IntegerValidator(string i)
        {
            string pattern = "[^0-9]";
            if (Regex.IsMatch(i, pattern))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void ClearTexts(string user, string pass)
        {
            user = String.Empty;
            pass = String.Empty;
        }
        
        internal bool IsLoggedIn(string user, string pass)
        {
            
            if (string.IsNullOrEmpty(user))
            {
                MessageBox.Show("Enter the user name!");
                return false;

            }            
            else if (StringValidator(user) == true)
            {
                MessageBox.Show("Enter only text here");
                ClearTexts(user, pass);
                return false;
            }           
            else
            {
                if (Username != user)
                {
                    MessageBox.Show("User name is incorrect!");
                    ClearTexts(user, pass);
                    return false;
                }               
                else
                {
                    if (string.IsNullOrEmpty(pass))
                    {
                        MessageBox.Show("Enter the passowrd!");
                        return false;
                    }               
                    else if (IntegerValidator(pass) == true)
                    {
                        MessageBox.Show("Enter only integer here");
                        return false;
                    }                  
                    else if (Password != pass)
                    {
                        MessageBox.Show("Password is incorrect");
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }

        }
    }
}