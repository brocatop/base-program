﻿namespace LogIn
{
    partial class RegisterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.createAccountButton = new System.Windows.Forms.Button();
            this.usernameLabel = new System.Windows.Forms.Label();
            this.emailAddress = new System.Windows.Forms.Label();
            this.confirmEmailAddressLabel = new System.Windows.Forms.Label();
            this.passwordLabel = new System.Windows.Forms.Label();
            this.confirmPasswordLabel = new System.Windows.Forms.Label();
            this.usernameBox = new System.Windows.Forms.TextBox();
            this.emailAddressBox = new System.Windows.Forms.TextBox();
            this.confirmEmailAddressBox = new System.Windows.Forms.TextBox();
            this.passwordBox = new System.Windows.Forms.TextBox();
            this.confirmPasswordBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // createAccountButton
            // 
            this.createAccountButton.Location = new System.Drawing.Point(295, 381);
            this.createAccountButton.Name = "createAccountButton";
            this.createAccountButton.Size = new System.Drawing.Size(153, 35);
            this.createAccountButton.TabIndex = 0;
            this.createAccountButton.Text = "Create Account";
            this.createAccountButton.UseVisualStyleBackColor = true;
            this.createAccountButton.Click += new System.EventHandler(this.CreateAccountButton_Click);
            // 
            // usernameLabel
            // 
            this.usernameLabel.AutoSize = true;
            this.usernameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usernameLabel.Location = new System.Drawing.Point(107, 115);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Size = new System.Drawing.Size(100, 20);
            this.usernameLabel.TabIndex = 1;
            this.usernameLabel.Text = "Username:";
            // 
            // emailAddress
            // 
            this.emailAddress.AutoSize = true;
            this.emailAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emailAddress.Location = new System.Drawing.Point(107, 153);
            this.emailAddress.Name = "emailAddress";
            this.emailAddress.Size = new System.Drawing.Size(137, 20);
            this.emailAddress.TabIndex = 2;
            this.emailAddress.Text = "Email Address:";
            // 
            // confirmEmailAddressLabel
            // 
            this.confirmEmailAddressLabel.AutoSize = true;
            this.confirmEmailAddressLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.confirmEmailAddressLabel.Location = new System.Drawing.Point(107, 196);
            this.confirmEmailAddressLabel.Name = "confirmEmailAddressLabel";
            this.confirmEmailAddressLabel.Size = new System.Drawing.Size(209, 20);
            this.confirmEmailAddressLabel.TabIndex = 3;
            this.confirmEmailAddressLabel.Text = "Confirm Email Address:";
            // 
            // passwordLabel
            // 
            this.passwordLabel.AutoSize = true;
            this.passwordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.passwordLabel.Location = new System.Drawing.Point(107, 242);
            this.passwordLabel.Name = "passwordLabel";
            this.passwordLabel.Size = new System.Drawing.Size(97, 20);
            this.passwordLabel.TabIndex = 4;
            this.passwordLabel.Text = "Password:";
            // 
            // confirmPasswordLabel
            // 
            this.confirmPasswordLabel.AutoSize = true;
            this.confirmPasswordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.confirmPasswordLabel.Location = new System.Drawing.Point(107, 285);
            this.confirmPasswordLabel.Name = "confirmPasswordLabel";
            this.confirmPasswordLabel.Size = new System.Drawing.Size(169, 20);
            this.confirmPasswordLabel.TabIndex = 5;
            this.confirmPasswordLabel.Text = "Confirm Password:";
            // 
            // usernameBox
            // 
            this.usernameBox.Location = new System.Drawing.Point(327, 113);
            this.usernameBox.Name = "usernameBox";
            this.usernameBox.Size = new System.Drawing.Size(258, 22);
            this.usernameBox.TabIndex = 6;
            this.usernameBox.TextChanged += new System.EventHandler(this.UsernameBox_TextChanged);
            this.usernameBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.UsernameBox_KeyDown_1);
            // 
            // emailAddressBox
            // 
            this.emailAddressBox.Location = new System.Drawing.Point(327, 151);
            this.emailAddressBox.Name = "emailAddressBox";
            this.emailAddressBox.Size = new System.Drawing.Size(258, 22);
            this.emailAddressBox.TabIndex = 7;
            this.emailAddressBox.TextChanged += new System.EventHandler(this.EmailAddressBox_TextChanged);
            this.emailAddressBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.EmailAddressBox_KeyDown_1);
            // 
            // confirmEmailAddressBox
            // 
            this.confirmEmailAddressBox.Location = new System.Drawing.Point(327, 196);
            this.confirmEmailAddressBox.Name = "confirmEmailAddressBox";
            this.confirmEmailAddressBox.Size = new System.Drawing.Size(258, 22);
            this.confirmEmailAddressBox.TabIndex = 8;
            this.confirmEmailAddressBox.TextChanged += new System.EventHandler(this.ConfirmEmailAddressBox_TextChanged);
            this.confirmEmailAddressBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ConfirmEmailAddressBox_KeyDown_1);
            // 
            // passwordBox
            // 
            this.passwordBox.Location = new System.Drawing.Point(327, 242);
            this.passwordBox.Name = "passwordBox";
            this.passwordBox.Size = new System.Drawing.Size(258, 22);
            this.passwordBox.TabIndex = 9;
            this.passwordBox.TextChanged += new System.EventHandler(this.PasswordBox_TextChanged);
            this.passwordBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PasswordBox_KeyDown_1);
            // 
            // confirmPasswordBox
            // 
            this.confirmPasswordBox.Location = new System.Drawing.Point(327, 285);
            this.confirmPasswordBox.Name = "confirmPasswordBox";
            this.confirmPasswordBox.Size = new System.Drawing.Size(258, 22);
            this.confirmPasswordBox.TabIndex = 10;
            this.confirmPasswordBox.TextChanged += new System.EventHandler(this.ConfirmPasswordBox_TextChanged);
            this.confirmPasswordBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ConfirmPasswordBox_KeyDown_1);
            // 
            // RegisterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.confirmPasswordBox);
            this.Controls.Add(this.passwordBox);
            this.Controls.Add(this.confirmEmailAddressBox);
            this.Controls.Add(this.emailAddressBox);
            this.Controls.Add(this.usernameBox);
            this.Controls.Add(this.confirmPasswordLabel);
            this.Controls.Add(this.passwordLabel);
            this.Controls.Add(this.confirmEmailAddressLabel);
            this.Controls.Add(this.emailAddress);
            this.Controls.Add(this.usernameLabel);
            this.Controls.Add(this.createAccountButton);
            this.Name = "RegisterForm";
            this.Text = "RegisterForm";
            this.Load += new System.EventHandler(this.RegisterForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button createAccountButton;
        private System.Windows.Forms.Label usernameLabel;
        private System.Windows.Forms.Label emailAddress;
        private System.Windows.Forms.Label confirmEmailAddressLabel;
        private System.Windows.Forms.Label passwordLabel;
        private System.Windows.Forms.Label confirmPasswordLabel;
        private System.Windows.Forms.TextBox usernameBox;
        private System.Windows.Forms.TextBox emailAddressBox;
        private System.Windows.Forms.TextBox confirmEmailAddressBox;
        private System.Windows.Forms.TextBox passwordBox;
        private System.Windows.Forms.TextBox confirmPasswordBox;
    }
}