﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Text.RegularExpressions;

namespace LogIn
{
    public partial class RegisterForm : Form
    {
        //Initializes the form
        public RegisterForm()
        {
            InitializeComponent();
        }

        //When the form loads, disables the create account button, and focsues the input to the username box
        private void RegisterForm_Load(object sender, EventArgs e)
        {
            createAccountButton.Enabled = false;
            usernameBox.Focus();
        }

        //Creates an account
        private void CreateAccountButton_Click(object sender, EventArgs e)
        {
            string u = usernameBox.Text;
            string p = passwordBox.Text;
            string eA = emailAddressBox.Text;
            string mc = @"Server=(local)\SQLEXPRESS; Database=Geno; integrated security=SSPI;";
            //If either the passwords or emails do not match, try again
            if (!confirmPasswordBox.Text.Equals(passwordBox.Text) || !confirmEmailAddressBox.Text.Equals(emailAddressBox.Text))
            {
                MessageBox.Show("Fields do not match, try again");
            }
            else
            {
                //Checks if a user already exists in the database. If it does, try again.

                SqlConnection sc = new SqlConnection(mc);
                SqlCommand connectExist = new SqlCommand("SELECT COUNT(*) from Users where Username like @u AND EmailAddress like @eA", sc);
                connectExist.Parameters.AddWithValue("@u", u);
                connectExist.Parameters.AddWithValue("@eA", eA);
                sc.Open();
                int userExists = (int)connectExist.ExecuteScalar();
                if (userExists > 0)
                {
                    MessageBox.Show("The Account already exists. Please choose a new account.");
                }
                //If the user does not already exist, and if it meets the apssword requirements, then add the user
                else if (VerifyPassword(p) == true)
                {
                    SqlCommand connectUpdate = new SqlCommand("INSERT INTO Users values (@Username, @Password, @Email)", sc);
                    connectUpdate.Parameters.AddWithValue("@Username", u);
                    connectUpdate.Parameters.AddWithValue("@Password", p);
                    connectUpdate.Parameters.AddWithValue("@Email", eA);
                    connectUpdate.ExecuteNonQuery();
                    sc.Close();
                    MessageBox.Show("An account has been successfully created. Sign in!");
                }
                else
                {
                    MessageBox.Show("The password does not meet the minimum requirements");
                }

            }

        }

        
        //Disables the create account button if the fields are empty. Enables the button when all of the fields have been filled
        private bool ValidateModuleDetails()
        {
            foreach (Control control in this.Controls)
                if (control.GetType() == typeof(TextBox))
                    if (!(String.IsNullOrEmpty(control.Text.Trim())) &&
                        !(String.IsNullOrWhiteSpace(control.Text.Trim())))
                    {
                        createAccountButton.Enabled = true;
                        return true;
                    }
                    else
                    {
                        createAccountButton.Enabled = false;
                        return false;
                    }
            return false;
        }

        //Utilize the validatemoduledetails for the textbox
        private void UsernameBox_TextChanged(object sender, EventArgs e)
        {
            ValidateModuleDetails();
        }

        //Utilize the validatemoduledetails for the textbox
        private void EmailAddressBox_TextChanged(object sender, EventArgs e)
        {
            ValidateModuleDetails();
        }

        //Utilize the validatemoduledetails for the textbox
        private void ConfirmEmailAddressBox_TextChanged(object sender, EventArgs e)
        {
            ValidateModuleDetails();
        }

        //Utilize the validatemoduledetails for the textbox. Hides the password with asterisks.
        private void PasswordBox_TextChanged(object sender, EventArgs e)
        {
            ValidateModuleDetails();
            passwordBox.PasswordChar = '*';
        }

        //Utilize the validatemoduledetails for the textbox. Hides the password with asterisks.
        private void ConfirmPasswordBox_TextChanged(object sender, EventArgs e)
        {
            ValidateModuleDetails();
            confirmPasswordBox.PasswordChar = '*';
        }

        //If the user presses "ENTER" while in the textbox -- moves to the next box
        private void UsernameBox_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                emailAddressBox.Focus();
            }
        }

        //If the user presses "ENTER" while in the textbox -- moves to the next box
        private void EmailAddressBox_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                confirmEmailAddressBox.Focus();
            }
        }

        //If the user presses "ENTER" while in the textbox -- moves to the next box
        private void ConfirmEmailAddressBox_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                passwordBox.Focus();
            }
        }

        //If the user presses "ENTER" while in the textbox -- moves to the next box
        private void PasswordBox_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                confirmPasswordBox.Focus();
            }
        }

        //If the user presses "ENTER" while in the textbox -- clicks the create account button
        private void ConfirmPasswordBox_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                createAccountButton.PerformClick();
            }
        }

        public static bool VerifyPassword(string password)
        {
            var hasNumber = new Regex(@"[0-9]+");
            var hasUpperChar = new Regex(@"[A-Z]+");
            var hasSpecialChars = new Regex(@"[!@#$%^&*()_+=\[{\]}:<>|./?,-]");
            var hasMinimum8Chars = new Regex(@".{8,}");

            bool isValidated = hasNumber.IsMatch(password) && hasUpperChar.IsMatch(password) && hasMinimum8Chars.IsMatch(password) && hasSpecialChars.IsMatch(password);
            return isValidated;
        }
    }
}
