﻿namespace LogIn
{
    partial class UploadFiles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SelectProfileLabel = new System.Windows.Forms.Label();
            this.FIleLabel = new System.Windows.Forms.Label();
            this.ImageLbael = new System.Windows.Forms.Label();
            this.ComboBox1 = new System.Windows.Forms.ComboBox();
            this.BrowseButton1 = new System.Windows.Forms.Button();
            this.BrowseButton2 = new System.Windows.Forms.Button();
            this.FileUploadText = new System.Windows.Forms.TextBox();
            this.ImageUploadText = new System.Windows.Forms.TextBox();
            this.UploadButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.FilesUploadedLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // SelectProfileLabel
            // 
            this.SelectProfileLabel.AutoSize = true;
            this.SelectProfileLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SelectProfileLabel.Location = new System.Drawing.Point(89, 69);
            this.SelectProfileLabel.Name = "SelectProfileLabel";
            this.SelectProfileLabel.Size = new System.Drawing.Size(124, 24);
            this.SelectProfileLabel.TabIndex = 0;
            this.SelectProfileLabel.Text = "Select Profile:";
            // 
            // FIleLabel
            // 
            this.FIleLabel.AutoSize = true;
            this.FIleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FIleLabel.Location = new System.Drawing.Point(167, 124);
            this.FIleLabel.Name = "FIleLabel";
            this.FIleLabel.Size = new System.Drawing.Size(46, 24);
            this.FIleLabel.TabIndex = 1;
            this.FIleLabel.Text = "FIle:";
            // 
            // ImageLbael
            // 
            this.ImageLbael.AutoSize = true;
            this.ImageLbael.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ImageLbael.Location = new System.Drawing.Point(146, 177);
            this.ImageLbael.Name = "ImageLbael";
            this.ImageLbael.Size = new System.Drawing.Size(67, 24);
            this.ImageLbael.TabIndex = 2;
            this.ImageLbael.Text = "Image:";
            // 
            // ComboBox1
            // 
            this.ComboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.ComboBox1.FormattingEnabled = true;
            this.ComboBox1.Items.AddRange(new object[] {
            ""});
            this.ComboBox1.Location = new System.Drawing.Point(227, 69);
            this.ComboBox1.Name = "ComboBox1";
            this.ComboBox1.Size = new System.Drawing.Size(205, 33);
            this.ComboBox1.TabIndex = 3;
            this.ComboBox1.Text = "Profiles";
            this.ComboBox1.SelectedIndexChanged += new System.EventHandler(this.ComboBox1_SelectedIndexChanged_1);
            // 
            // BrowseButton1
            // 
            this.BrowseButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BrowseButton1.Location = new System.Drawing.Point(357, 124);
            this.BrowseButton1.Name = "BrowseButton1";
            this.BrowseButton1.Size = new System.Drawing.Size(83, 33);
            this.BrowseButton1.TabIndex = 4;
            this.BrowseButton1.Text = "Browse";
            this.BrowseButton1.UseVisualStyleBackColor = true;
            this.BrowseButton1.Click += new System.EventHandler(this.BrowseButton1_Click);
            // 
            // BrowseButton2
            // 
            this.BrowseButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BrowseButton2.Location = new System.Drawing.Point(357, 177);
            this.BrowseButton2.Name = "BrowseButton2";
            this.BrowseButton2.Size = new System.Drawing.Size(83, 33);
            this.BrowseButton2.TabIndex = 5;
            this.BrowseButton2.Text = "Browse";
            this.BrowseButton2.UseVisualStyleBackColor = true;
            this.BrowseButton2.Click += new System.EventHandler(this.BrowseButton2_Click);
            // 
            // FileUploadText
            // 
            this.FileUploadText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FileUploadText.Location = new System.Drawing.Point(227, 125);
            this.FileUploadText.Name = "FileUploadText";
            this.FileUploadText.Size = new System.Drawing.Size(124, 30);
            this.FileUploadText.TabIndex = 6;
            // 
            // ImageUploadText
            // 
            this.ImageUploadText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ImageUploadText.Location = new System.Drawing.Point(227, 180);
            this.ImageUploadText.Name = "ImageUploadText";
            this.ImageUploadText.Size = new System.Drawing.Size(124, 30);
            this.ImageUploadText.TabIndex = 7;
            // 
            // UploadButton
            // 
            this.UploadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UploadButton.Location = new System.Drawing.Point(171, 252);
            this.UploadButton.Name = "UploadButton";
            this.UploadButton.Size = new System.Drawing.Size(98, 31);
            this.UploadButton.TabIndex = 8;
            this.UploadButton.Text = "Upload";
            this.UploadButton.UseVisualStyleBackColor = true;
            this.UploadButton.Click += new System.EventHandler(this.UploadButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CancelButton.Location = new System.Drawing.Point(321, 252);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(93, 31);
            this.CancelButton.TabIndex = 9;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // FilesUploadedLabel
            // 
            this.FilesUploadedLabel.AutoSize = true;
            this.FilesUploadedLabel.Location = new System.Drawing.Point(240, 292);
            this.FilesUploadedLabel.Name = "FilesUploadedLabel";
            this.FilesUploadedLabel.Size = new System.Drawing.Size(102, 17);
            this.FilesUploadedLabel.TabIndex = 10;
            this.FilesUploadedLabel.Text = "Files Uploaded";
            // 
            // UploadFiles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(608, 318);
            this.Controls.Add(this.FilesUploadedLabel);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.UploadButton);
            this.Controls.Add(this.ImageUploadText);
            this.Controls.Add(this.FileUploadText);
            this.Controls.Add(this.BrowseButton2);
            this.Controls.Add(this.BrowseButton1);
            this.Controls.Add(this.ComboBox1);
            this.Controls.Add(this.ImageLbael);
            this.Controls.Add(this.FIleLabel);
            this.Controls.Add(this.SelectProfileLabel);
            this.Name = "UploadFiles";
            this.Text = "UploadFiles";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label SelectProfileLabel;
        private System.Windows.Forms.Label FIleLabel;
        private System.Windows.Forms.Label ImageLbael;
        private System.Windows.Forms.ComboBox ComboBox1;
        private System.Windows.Forms.Button BrowseButton1;
        private System.Windows.Forms.Button BrowseButton2;
        private System.Windows.Forms.TextBox FileUploadText;
        private System.Windows.Forms.TextBox ImageUploadText;
        private System.Windows.Forms.Button UploadButton;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.Label FilesUploadedLabel;
    }
}